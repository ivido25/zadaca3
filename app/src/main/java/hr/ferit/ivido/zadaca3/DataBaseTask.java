package hr.ferit.ivido.zadaca3;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;




public class DataBaseTask extends SQLiteOpenHelper {
    private static final int SCHEMA             = 1;
    private static final String DATABASE_NAME   = "tasks.db";

    //A table to store owned cars:
    static final String TABLE_MY_TASKS   = "my_tasks";
    static final String PRIORITY         = "priority";
    static final String TEXT             = "text";
    static final String CATEGORY         = "category";

    //There should be only one instance of CarDBHelper:
    private static DataBaseTask mInstance = null;
    //The constructor should be kept private:
    private DataBaseTask (Context context)
    {
        super(context,DATABASE_NAME,null,SCHEMA);
    }
    //And the public method to access the instance:
    public static synchronized DataBaseTask getInstance(Context context)
    {
        if(null == mInstance)
        {
            context = context.getApplicationContext();
            mInstance = new DataBaseTask(context);
        }
        return  mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String CREATE_TABLE_MY_CARS =
                "CREATE TABLE "+TABLE_MY_TASKS+
                        " ("+TEXT+" TEXT,"+CATEGORY+" TEXT,"+PRIORITY+" INTEGER);";
        db.execSQL(CREATE_TABLE_MY_CARS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        final String DROP_TABLE_MY_CARS = "DROP TABLE IF EXISTS "+TABLE_MY_TASKS;
        db.execSQL(DROP_TABLE_MY_CARS);
        onCreate(db);
    }

    // This method (and all database transactions) should be called on a separate thread,
    // for example asyncTask, or a simple Thread
   public ArrayList<Task> retrieveMyTasks()
    {
        // Usage of a raw query:
        String SELECT_ALL_TASKS = "SELECT "+TEXT+","
                +CATEGORY+","+PRIORITY+" FROM "+TABLE_MY_TASKS;
        ArrayList<Task> myTasks = new ArrayList<>();
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor myTasksCursor = database.rawQuery(SELECT_ALL_TASKS, null);
        if(myTasksCursor.moveToFirst())
        {
            do {
                Task task = new Task(
                        myTasksCursor.getString(1),
                        myTasksCursor.getString(0),
                        myTasksCursor.getInt(2)
                );
                myTasks.add(task);
            }while(myTasksCursor.moveToNext());
        }
        myTasksCursor.close();
        database.close();
        return myTasks;
    }

    public void insertTask(Task task) {
        //Using the insert method of the database object:
        ContentValues carValues = new ContentValues();
        carValues.put(TEXT,task.getText());
        carValues.put(CATEGORY,task.getCategory());
        carValues.put(PRIORITY,task.getPriority());

        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_MY_TASKS,TEXT,carValues);
        db.close();
    }
    public void deleteTask(Task task){

        String name = task.getText();
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_MY_TASKS, TEXT + "='" + name+"'", null);
    }
}

