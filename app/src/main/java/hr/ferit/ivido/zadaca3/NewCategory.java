package hr.ferit.ivido.zadaca3;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NewCategory extends AppCompatActivity implements View.OnClickListener {


    Button bAdd;
    EditText etEnter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_category);

        this.bAdd = (Button) findViewById(R.id.bAdd);
        this.etEnter = (EditText) findViewById(R.id.etEnterNewCat);

        bAdd.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("key_get",etEnter.getText().toString());
        setResult(RESULT_OK,returnIntent);
        finish();
    }
}
