package hr.ferit.ivido.zadaca3;

public class Task {

    private String category, text;
    private int priority;


    public Task(String category, String text, int priority) {
        this.category=category;
        this.text=text;
        switch (priority){
            case(1): this.priority = R.drawable.orange;
                break;
            case(2): this.priority = R.drawable.red;
                break;
            case(R.drawable.orange): this.priority = R.drawable.orange;
                break;
            case(R.drawable.red): this.priority = R.drawable.red;
                break;
            default: this.priority = R.drawable.green;
                break;
        }

    }

    public String getCategory() {
        return category;
    }

    public String getText() {
        return text;
    }

    public int getPriority() {
        return priority;
    }
}
