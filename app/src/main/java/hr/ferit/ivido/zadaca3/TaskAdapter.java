package hr.ferit.ivido.zadaca3;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

class TaskAdapter extends BaseAdapter {

    ArrayList<Task> mTasks;

    public TaskAdapter(ArrayList<Task> mTasks){this.mTasks=mTasks;}

    @Override
    public int getCount() {
        return this.mTasks.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mTasks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TaskViewHolder taskViewHolder;
        Context parentContext = parent.getContext();

        if(null==convertView)
        {
            convertView=View.inflate(parentContext,R.layout.task_layout,null);

            taskViewHolder=new TaskViewHolder();
            taskViewHolder.ivPriority=(ImageView) convertView.findViewById(R.id.ivPriority);
            taskViewHolder.tvCategory=(TextView) convertView.findViewById(R.id.tvCategory);
            taskViewHolder.tvText=(TextView) convertView.findViewById(R.id.tvTaskText);

            convertView.setTag(taskViewHolder);
        }
        else{
            taskViewHolder =(TaskViewHolder) convertView.getTag();
        }
        Task currentTask = this.mTasks.get(position);
        //debug
        //int test = currentTask.getPriority();
        //
        taskViewHolder.ivPriority.setImageResource(currentTask.getPriority());
        taskViewHolder.tvCategory.setText(currentTask.getCategory());
        taskViewHolder.tvText.setText(currentTask.getText());

        return convertView;
    }

    public void add(Task task) {
        this.mTasks.add(task);
        this.notifyDataSetChanged();
    }

    public void remove(int position) {
        this.mTasks.remove(position);
        this.notifyDataSetChanged();
    }

    static class TaskViewHolder
    {
        private ImageView ivPriority;
        private TextView tvText;
        private TextView tvCategory;
    }
}
