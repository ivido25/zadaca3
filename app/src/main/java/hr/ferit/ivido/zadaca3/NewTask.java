package hr.ferit.ivido.zadaca3;

import android.content.Intent;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;

import hr.ferit.ivido.zadaca3.DataBaseTask;

public class NewTask extends AppCompatActivity implements View.OnClickListener {

    Button bAdd;
    EditText etEnter;
    Spinner spinCat;
    public Spinner spinPrio;
    DataBaseTask mDataBaseTask;
    TaskAdapter adapter;
    ArrayList<Task> mTasks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_task);
        initUI();
    }

    private void initUI() {

        this.mDataBaseTask = mDataBaseTask.getInstance(this);
        this.mTasks=mDataBaseTask.retrieveMyTasks();
        adapter=new TaskAdapter(mTasks);


        bAdd = (Button) findViewById(R.id.bAdd);
        etEnter =(EditText) findViewById(R.id.etEnterNewTask);
        spinCat =(Spinner) findViewById(R.id.spinCategory);
        spinPrio =(Spinner) findViewById(R.id.spinPriority);

        Intent intent = getIntent();
        String[] spinnerArr = intent.getStringArrayExtra("key_string");
        //String addCat = intent.getStringExtra("key_add");
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArr);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinCat.setAdapter(spinnerAdapter);
        //spinnerAdapter.add(addCat);
        spinnerAdapter.notifyDataSetChanged();

        bAdd.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        //int test = spinPrio.getSelectedItemPosition();
        Task task = new Task(
                spinCat.getSelectedItem().toString(),
                etEnter.getText().toString(),
                spinPrio.getSelectedItemPosition()
        );
        mDataBaseTask.insertTask(task);
        adapter.add(task);

        finish();
    }
}
