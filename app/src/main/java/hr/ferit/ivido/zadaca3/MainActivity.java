package hr.ferit.ivido.zadaca3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemLongClickListener, View.OnClickListener {

    private static final int REQUEST_CAT = 1;
    //private static final int REQUEST_TASK=2;
    private String[] spinnerArr = new String[]{"Work","Hobby","Other","Shopping"};
    private String addCat = null;

    private Button bNewTask,bNewCategory;
    private ListView lvTask;
    private ArrayList<Task> mTasks;
    private TaskAdapter mTaskAdapter;
    private DataBaseTask mDataBaseTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.mDataBaseTask = mDataBaseTask.getInstance(this);
        this.mTasks=mDataBaseTask.retrieveMyTasks();
        this.mTaskAdapter=new TaskAdapter(this.mTasks);
        this.lvTask.setAdapter(this.mTaskAdapter);
    }

    private void initUI() {
        this.mDataBaseTask = mDataBaseTask.getInstance(this);

        this.bNewTask = (Button) findViewById(R.id.bAddNewTask);
        this.bNewCategory=(Button) findViewById(R.id.bAddNewCategory);
        this.lvTask = (ListView) findViewById(R.id.lvDatabaseItems);

        lvTask.setOnItemLongClickListener(this);
        bNewCategory.setOnClickListener(this);
        bNewTask.setOnClickListener(this);

        this.mTasks=mDataBaseTask.retrieveMyTasks();
        this.mTaskAdapter=new TaskAdapter(this.mTasks);
        this.lvTask.setAdapter(this.mTaskAdapter);
    }


    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        TaskAdapter adapter = (TaskAdapter) parent.getAdapter();
        Task selectedTask = (Task) adapter.getItem(position);
        adapter.remove(position);
        mDataBaseTask.deleteTask(selectedTask);
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case(R.id.bAddNewCategory):{
                Intent intent = new Intent(this, NewCategory.class);
                startActivityForResult(intent,REQUEST_CAT);
            }
                break;
            case(R.id.bAddNewTask):
            {
                Intent intent = new Intent(this, NewTask.class);
                intent.putExtra("key_string",spinnerArr);
                //if(addCat!=null){
                //intent.putExtra("key_add",addCat);}
                startActivity(intent);
            }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==REQUEST_CAT){
            if(resultCode==RESULT_OK){
                addCat=data.getStringExtra("key_get");
                //Toast.makeText(getApplicationContext(), addCat, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
